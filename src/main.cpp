#include <Arduino.h>
#include <TFT_eSPI.h>       // Hardware-specific library

#include <SPIFFS.h>
#include <Wifi.h>
#include <QRDisplayConfig.h>
#include <HTTPClient.h>
#include <string>
#include <Button.h>
#include "../lib/qrcode/QRCodeContent.h"
#include "../lib/qrcode/QRCodeTFTPrinter.h"
#include "ResultDisplayContent.h"
#include "WifiStatusDisplayContent.h"
#include "../lib/display/LayeredContent.h"
#include "../lib/display/Display.h"


using namespace std;

TFT_eSPI tft;

Wifi wifi;
QRDisplayConfig config;

ResultDisplayContent resultDisplayContent;
WifiStatusDisplayContent wifiStatusDisplayContent(&wifi);
LayeredContent displayContent(&wifiStatusDisplayContent,NULL);
Display display(&tft, &displayContent);

Button button(35);

void show(DisplayContent *content) {
    displayContent.l2 = content;
    display.invalidateContent();
}

string readUrl(string result){
    DynamicJsonDocument jsonDocument(2048);
    deserializeJson(jsonDocument,result);
    const char * r = jsonDocument["url"];
    return string(r);
}

void getLast() {
    if(wifi.isConnected()){
        HTTPClient http;
        string url = string(config.backend());
        http.begin(url.c_str());
        http.addHeader("Authorization", config.authorization());
        int returnCode = http.GET();
        string resp = string(http.getString().c_str());
        Serial.println(resp.c_str());
        if(returnCode == 200 && resp.size() > 2){
            resultDisplayContent = ResultDisplayContent(readUrl(resp));
            show(&resultDisplayContent);
        } else {
            Serial.println(("Error while submitting score : " + to_string(returnCode) + " : " + resp).c_str());
        }
    }
}


void setup() {
    Serial.begin(115200);

    //has to be in main.cpp or fail
    if (!SPIFFS.begin()) {
        throw std::runtime_error("An Error has occurred while mounting SPIFFS");
    }
    config.setup();

    tft.init();
    wifi.setup(config.ssid(), config.wifiPassword(), config.hostname());

    pinMode(33, OUTPUT);
    digitalWrite(33, HIGH);
    button.setup();

}

void loop() {
    wifi.run();
    if(!wifiStatusDisplayContent.valid()){
        display.invalidateContent();
    }
    display.run();
    if(button.popClicked()){
        getLast();
    }

}