

#pragma once


#include "../lib/display/DisplayContent.h"
#include <string>

using namespace std;

class ResultDisplayContent : public DisplayContent {
public:
    ResultDisplayContent(string url="");

    void fill(TFT_eSPI *display) override;

private:

    string url;

};



