

#include "ResultDisplayContent.h"
#include "../lib/display/Display.h"
#include "../lib/qrcode/QRCodeTFTPrinter.h"

ResultDisplayContent::ResultDisplayContent(string url) : url(url) {}

void ResultDisplayContent::fill(TFT_eSPI *tft) {

    tft->fillScreen(Display::WHITE);

    QRCodeContent code = QRCodeContent(url);
    QRCodeTFTPrinter::paint(tft, &code, 118, 5, (160 - 118) / 2);
}
