

#pragma once

#include <string>
#include <qrcode.h>
#include <stdlib.h>

using namespace std;


class QRCodeContent {
private:
    static const int VERSION_MAX_ALPHA_ECC_MEDIUM[40];
    string content;
    QRCode qrcode;
    uint8_t *qrcodeBytes;
    int version;

// Allocate a chunk of memory to store the QR code

public:
    QRCodeContent(string content);

    ~QRCodeContent();



    int sizeInSquare() {
        return 4 * version + 17;
    }

    bool fill(int x, int y);

};





