

#pragma once


#include "DisplayContent.h"

class Display {
public:
    Display(TFT_eSPI *tft, DisplayContent *content = NULL);

    static const int RED = TFT_BLUE;
    static const int BLUE = TFT_RED;
    static const int GREEN = TFT_GREEN;
    static const int WHITE = TFT_WHITE;
    static const int BLACK = TFT_BLACK;
    void run();
    void invalidateContent();

private:
    DisplayContent *content;
    TFT_eSPI *tft;
    bool contentValid = false;


};



